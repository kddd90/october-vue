import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    vehicles: [],
    locations: [],
    filteredVehicles: []
  },
  getters: {
    vehicles: state => state.vehicles,
    locations: state => state.locations,
    filterdVehicles: state => state.filteredVehicles
  },
  mutations: {
    SET_VEHICLES: (state, vehicles) => {
      state.vehicles = vehicles
    },
    SET_LOCATIONS: (state, locations) => {
      state.locations = locations
    },
    SET_FILTERED: (state, vehicles) => {
      state.filteredVehicles = vehicles
    }
  },
  actions: {
    getVehicles ({ commit }) {
      const uri = 'http://api.rentacar.local/vehicles'
      axios.get(uri).then(res => {
        commit('SET_VEHICLES', res.data)
        commit('SET_FILTERED', res.data)
      })
    },
    getLocations ({ commit }) {
      const uri = 'http://api.rentacar.local/locations'
      axios.get(uri).then(res => {
        if (res.data.length) {
          res.data = [...[{ value: 0, text: '--Chọn địa điểm--' }], ...res.data.map((item, index) => {
            return Object.assign({}, {
              value: item.id,
              text: item.title
            }, item)
          })]
        }
        commit('SET_LOCATIONS', res.data)
      })
    },
    filterVehicles ({ commit, state }, value) {
      if (value) {
        const filtered = state.vehicles.filter(vehicle => {
          const foundLocations = vehicle.locations.findIndex(location => {
            return location.id === value
          })

          return foundLocations !== -1
        })
        commit('SET_FILTERED', filtered)
      } else {
        commit('SET_FILTERED', state.vehicles)
      }
    },
    filterOnApi ({ commit }, value) {
      axios.get('http://api.rentacar.local/vehicles/filter/' + value).then(response => {
        commit('SET_FILTERED', response.data)
      })
    }
  }
})
