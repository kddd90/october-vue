<?php namespace Troi\Shop\Models;

use Model;

/**
 * Model
 */
class Location extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'troi_shop_locations';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
