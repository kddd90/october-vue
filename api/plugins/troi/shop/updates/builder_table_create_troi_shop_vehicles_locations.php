<?php namespace Troi\Shop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTroiShopVehiclesLocations extends Migration
{
    public function up()
    {
        Schema::create('troi_shop_vehicles_locations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('vehicle_id');
            $table->integer('location_id');
            $table->primary(['vehicle_id','location_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('troi_shop_vehicles_locations');
    }
}
